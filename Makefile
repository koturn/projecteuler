### This Makefile was written for GNU Make. ###
CXX     = g++
C_WARNING_FLAGS = -Wall -Wextra -Wformat=2 -Wstrict-aliasing=2  \
                  -Wcast-align -Wcast-qual -Wconversion         \
                  -Wfloat-equal -Wpointer-arith -Wswitch-enum   \
                  -Wwrite-strings
CXX_WARNING_FLAGS = $(C_WARNING_FLAGS) -Weffc++ -Woverloaded-virtual

ifeq ($(MP),true)
    LDFLAGS = -pipe -O3 -s -fopenmp $(CXX_WARNING_FLAGS)
else
    LDFLAGS = -pipe -O3 -s $(CXX_WARNING_FLAGS) -Wno-unknown-pragmas
endif


%.exe:
	$(CXX) $(LDFLAGS) $(filter %.cc %.cpp %.o, $^) $(LDLIBS) -o $@
%.out:
	$(CXX) $(LDFLAGS) $(filter %.cc %.cpp %.o, $^) $(LDLIBS) -o $@


.PHONY: clean
clean :
	$(RM) *.exe
