#include "util.h"

std::string itos(uint n);
bool        cheak_palindrome(std::string str);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint MAX = 999;
  static const uint MIN = 100;
  uint largest = 0;
  for (uint i = MAX; i >= MIN; i--) {
    for (uint j = i; j >= MIN; j--) {
      uint p = i * j;
      if (p <= largest) {
        break;
      }
      std::string nstr = itos(p);
      if (cheak_palindrome(nstr)) {
        std::printf("%u * %u = %u\n", i, j, p);
        largest = p;
      }
    }
  }
  std::printf("answer = %u\n", largest);
  return 0;
}


std::string
itos(uint n)
{
  std::stringstream ss;
  ss << n;
  return ss.str();
}


bool
cheak_palindrome(std::string str)
{
  for (uint i = 0, j = str.length() - 1; i <= j; i++, j--) {
    if (str[i] != str[j]) {
      return false;
    }
  }
  return true;
}
