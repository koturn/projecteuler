#include "util.h"

static const uint ROW = 20;
static const uint COL = 20;

uint
search(uint array[ROW][COL], uint s);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);
  static uint array[ROW][COL];

  // assign number to array
  FOREACH_SA(row, array) FOREACH_SA(elm, *row) {
    uint n;
    std::cin >> n;
    *elm = n;
  }

  uint max = search(array, 4);
  std::printf("answer = %u", max);
  return EXIT_SUCCESS;
}


uint
search(uint array[ROW][COL], uint s)
{
  uint max = 0;
  // left to right
  REP (i, ROW) REP (j, COL + 1 - s) {
    uint p = 1;
    REP (k, s) {
      p *= array[i][j + k];
    }
    if (p > max) {
      max = p;
    }
  }

  // up to down
  REP (i, ROW + 1 - s) REP (j, COL) {
    uint p = 1;
    REP (k, s) {
      p *= array[i + k][j];
    }
    if (p > max) {
      max = p;
    }
  }

  // upper left to lower right
  REP (i, ROW + 1 - s) REP (j, COL + 1 - s) {
    uint p = 1;
    REP (k, s) {
      p *= array[i + k][j + k];
    }
    if (p > max) {
      max = p;
    }
  }

  // upper right to lower left
  REP (i, ROW + 1 - s) FOR (j, s - 1, COL + 1 - s) {
    uint p = 1;
    REP (k, s) {
      p *= array[i + (s - k - 1)][j + k];
    }
    if (p > max) {
      max = p;
    }
  }
  return max;
}
