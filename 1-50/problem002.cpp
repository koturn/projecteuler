#include "util.h"

static const uint LIMIT = 4000000;


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  uint n = 1, n1 = 1, n2 = 1;
  uint even_sum = 0;
  while (n1 <= LIMIT) {
    std::cout << n << std::endl;
    n = n1 + n2;
    if ((n & 1) == 0) {
      even_sum += n;
    }
    n2 = n1;
    n1 = n;
  }
  std::cout << "fibonnaci even_sum = " << even_sum << std::endl;
  return 0;
}
