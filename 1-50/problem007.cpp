#include "util.h"

uint
serach_prime(uint s);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint S = 10001;
  uint p = serach_prime(S);
  std::printf("answer = %u\n", p);
  return 0;
}


uint
serach_prime(uint s)
{
  static const uint LIMIT = 100000000;
  uint cnt;

  if (s == 1) {
    return 2;
  } else if (s == 2) {
    return 3;
  } else {
    std::vector<uint> primes(1, 3);
    cnt = 2;
    for (uint i = 5; i <= LIMIT; i += 2) {
      bool is_prime = true;

      // identify with prime table.
      for (uint j = 0, max = static_cast<uint>(std::sqrt(i)); primes[j] <= max; j++) {
        if (i % primes[j] == 0) {
          is_prime = false;
          break;
        }
      }
      // add to prime table.
      if (is_prime) {
        if (++cnt == s) {
          return i;
        }
        primes.push_back(i);
      }
    }
  }
  return 0;
}
