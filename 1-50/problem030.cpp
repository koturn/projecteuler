#include "util.h"

std::vector<uint>
get_each_digits(uint n);

inline static uint
funcion(uint n);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint N = 1000000;
  uint sum = 0;

  #pragma omp parallel for reduction(+: sum)
  FOR (i, 2, N) {
    uint dsum = 0;

    std::vector<uint> digits = get_each_digits(i);
    FOREACH (elm, digits) {
      dsum += funcion(*elm);
    }
    if (i == dsum) {
      DUMP(i);
      sum += dsum;
    }
  }
  std::cout << "answer = " << sum << std::endl;

  return EXIT_SUCCESS;
}


std::vector<uint>
get_each_digits(uint n)
{
  std::vector<uint> digits;
  while (n != 0) {
    digits.push_back(n % 10);
    n /= 10;
  }
  return digits;
}


inline static uint
funcion(uint n)
{
  return n * n * n * n * n;
}
