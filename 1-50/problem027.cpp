#include "util.h"

bool
is_prime(uint n);

inline static int
function(int n, int a, int b);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  const static int MIN = -1000;
  const static int MAX =  1000;
  int max_a = 0, max_b = 0;
  int max_n = 0;

  #pragma omp parallel for
  FOR (i, MIN, MAX) FOR (j, MIN, MAX) {
    int k = -1;
    while (k++, is_prime(function(k, i, j)));
    if (max_n < k) {
      max_a = i;
      max_b = j;
      max_n = k;
    }
  }
  std::cout << "a = " << max_a << ", b = " << max_b << std::endl;
  std::cout << "n = " << max_n << std::endl;
  std::cout << "answer = " << (max_a * max_b) << std::endl;
  return EXIT_SUCCESS;
}


bool
is_prime(uint n)
{
  if ((n & 1) == 0) return false;
  for (uint i = 3, max = std::sqrt(n); i <= max; i += 2) {
    if (n % i == 0) return false;
  }
  return true;
}


inline static int
function(int n, int a, int b)
{
  return n * n + a * n + b;
}
