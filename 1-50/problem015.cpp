#include "util.h"


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint N = 20;
  uint cmb1[N], cmb2[N];
  #pragma omp parallel sections
  {
    #pragma omp section
    REP (i, LENGTH(cmb1)) cmb1[i] = i + N + 1;
    #pragma omp section
    REP (i, LENGTH(cmb2)) cmb2[i] = i + 1;
  }

  #pragma omp parallel for
  REP (i, LENGTH(cmb1)) FOR (j, 1, LENGTH(cmb2)) {
    if (cmb1[i] % cmb2[j] == 0) {
      cmb1[i] /= cmb2[j];
      cmb2[j] = 1;
    }
  }

  lluint p1 = 1, p2 = 1;

  #pragma omp parallel sections
  {
    #pragma omp section
    FOREACH_SA(elm, cmb1) p1 *= *elm;
    // REP (i, LENGTH(cmb1)) p1 *= cmb1[i];
    #pragma omp section
    FOREACH_SA(elm, cmb2) p2 *= *elm;
    // REP (i, LENGTH(cmb2)) p2 *= cmb2[i];
  }

  std::cout << "answer = " << (p1 / p2) << std::endl;
  return EXIT_SUCCESS;
}
