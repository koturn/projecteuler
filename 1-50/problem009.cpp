#include "util.h"


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint N = 1000;
  #pragma omp parallel for
  for (uint i = N / 2; i > 0; i--) {
    for (uint j = i - 1; j > 0; j--) {
      uint k = N - i - j;
      if (SQ(i) + SQ(j) == SQ(k)) {
        std::printf("a * b * c = %u * %u * %u = %u\n", i, j, k, i * j * k);
      }
    }
  }
  return 0;
}
