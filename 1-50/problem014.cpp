#include "util.h"


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint N = 1000000;
  uint max = 0;
  uint n   = 0;

  #pragma omp parallel for
  for (uint i = N - 1; i > 0; i--) {
    uint m = i;
    uint cnt = 0;
    while (m != 1) {
      cnt++;
      if (m & 1) {  // m is odd
        m = m * 3 + 1;
      } else {      // m is even
        m >>= 1;
      }
    }
    if (max < cnt) {
      max = cnt;
      n = i;
    }
  }
  std::printf("answer = %u\n", n);
  return EXIT_SUCCESS;
}
