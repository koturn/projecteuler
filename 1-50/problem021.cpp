#include "util.h"

uint
calc_sum_divisors(uint n);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint N = 10000;
  uint sum = 0;

  #pragma omp parallel for
  FOR (i, 2, N) {
    uint j = calc_sum_divisors(i);
    if (i < j && i == calc_sum_divisors(j)) {
      sum += i + j;
      std::printf("%u : %u\n", i, j);
    }
  }
  std::printf("answer = %u\n", sum);
  return EXIT_SUCCESS;
}


uint
calc_sum_divisors(uint n)
{
  uint sum = 0;
  for (uint i = 1, limit = n / 2; i <= limit; i++) {
    if (n % i == 0) {
      sum += i;
    }
  }
  return sum;
}
