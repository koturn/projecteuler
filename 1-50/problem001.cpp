#include "util.h"


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  uint sum = 0;
  #pragma omp parallel for reduction(+:sum)
  FOR (i, 1, 1000) {
    if (i % 3 == 0 || i % 5 == 0) {
      sum += i;
    }
  }
  std::cout << sum << std::endl;
  return 0;
}
