#include "util.h"

lluint
calc_sum1(uint n);

lluint
calc_sum2(uint n);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint N = 100;
  lluint sum1 = calc_sum1(N);
  lluint sum2 = calc_sum2(N);
  std::cout << "sum1 = " << sum1 << std::endl;
  std::cout << "sum2 = " << sum2 << std::endl;
  std::cout << "answer = " << (sum2 - sum1) << std::endl;

  return 0;
}


lluint
calc_sum1(uint n)
{
  uint sum = 0;
  #pragma omp parallel for reduction(+:sum)
  FOR (i, 1, n + 1) {
    sum += SQ(i);
  }
  return sum;
}


lluint
calc_sum2(uint n)
{
  uint sum = 0;
  #pragma omp parallel for reduction(+:sum)
  FOR (i, 1, n + 1) {
    sum += i;
  }
  return SQ(sum);
}
