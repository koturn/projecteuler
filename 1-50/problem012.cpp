#include "util.h"

uint
calc_nfact(uint n);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint CNT_LIMIT = 500;
  uint n = 1;
  uint triangle_number = 0;
  while (calc_nfact(triangle_number = (n * (n + 1) / 2)) <= CNT_LIMIT) n++;
  std::printf("answer = %u\n", triangle_number);
  return EXIT_SUCCESS;
}


uint
calc_nfact(uint n)
{
  uint nfact = 1;
  uint cnt = 1;
  while ((n & 1) == 0) {
    cnt++;
    n >>= 1;
  }
  nfact *= cnt;

  for (uint i = 3; i <= n; i += 2) {
    cnt = 1;
    while (n % i == 0) {
      cnt++;
      n /= i;
    }
    nfact *= cnt;
  }
  return nfact;
}
