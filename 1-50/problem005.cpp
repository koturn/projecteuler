#include "util.h"

std::vector<uint>
make_prime_table(uint n);

std::map<uint, uint>
make_pf_map(uint n, const std::vector<uint> &prime_table);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint N = 20;
  std::vector<uint> prime_table = make_prime_table(N);
  std::map<uint, uint> pf_map   = make_pf_map(N, prime_table);

  uint p = 1;
  FOREACH (prime, prime_table) {
    std::printf("%u : %u\n", *prime, pf_map[*prime]);
    LOOP (pf_map[*prime]) {
      p *= *prime;
    }
  }
  std::printf("answer = %u\n", p);
  return 0;
}


std::vector<uint>
make_prime_table(uint n)
{
  std::vector<uint> prime_table;
  if (n < 3) {
    prime_table.push_back(2);
  } else {
    prime_table.push_back(3);
    for (uint i = 5; i <= n; i += 2) {
      bool is_prime = true;

      // identify with prime table.
      for (uint j = 0, max = static_cast<uint>(sqrt(i)); prime_table[j] <= max; j++) {
        if (i % prime_table[j] == 0) {
          is_prime = false;
          break;
        }
      }
      // add to prime table.
      if (is_prime) {
        prime_table.push_back(i);
      }
    }
    prime_table.insert(prime_table.begin(), 2);
  }
  return prime_table;
}


std::map<uint, uint>
make_pf_map(uint n, const std::vector<uint> &prime_table)
{
  std::map<uint, uint> pf_map;
  FOREACH (prime, prime_table) {
    pf_map.insert(std::map<uint, uint>::value_type(*prime, 0));
  }

  FOR (i, 2, n + 1) {
    uint cnt = 0;
    uint m = i;
    while ((m & 1) == 0) {
      cnt++;
      m >>= 1;
    }
    if (pf_map[2] < cnt) {
      pf_map[2] = cnt;
    }

    for (uint j = 3; j <= m; j += 2) {
      cnt = 0;
      while (m % j == 0) {
        cnt++;
        m /= j;
      }
      if (pf_map[j] < cnt) {
        pf_map[j] = cnt;
      }
    }
  }
  return pf_map;
}
