#include "util.h"

// static const uint N = 13195;
static const lluint N = 600851475143;


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  lluint n = N;
  std::vector<lluint> prime_factors;
  while ((n & 1) == 0) {
    prime_factors.push_back(2);
    n >>= 1;
  }
  for (lluint i = 3, limit = static_cast<uint>(sqrt(n)); i <= limit; i += 2) {
    while (n % i == 0) {
      prime_factors.push_back(i);
      n /= i;
    }
  }
  FOREACH (elm, prime_factors) {
    std::cout << *elm << std::endl;
  }
  return 0;
}
