#include "util.h"

lluint
calc_prime_sum(uint n);


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  static const uint N = 2000000;
  std::cout << "answer = " << calc_prime_sum(N) << std::endl;
  return 0;
}


lluint
calc_prime_sum(uint n)
{
  if (n < 2) {
    return 0;
  } else if (n < 3) {
    return 2;
  } else {
    lluint sum = 5;
    std::vector<uint> primes(1, 3);
    for (uint i = 5; i <= n; i += 2) {
      bool is_prime = true;

      // identify with prime table.
      for (uint j = 0, limit = static_cast<uint>(std::sqrt(i)); primes[j] <= limit; j++) {
        if (i % primes[j] == 0) {
          is_prime = false;
          break;
        }
      }
      // add to prime table.
      if (is_prime) {
        sum += i;
        primes.push_back(i);
      }
    }
    return sum;
  }
}
