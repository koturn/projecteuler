#include "util.h"

uint
search(uint depth, uint col);

static const uint N = 15;
static uint array[N][N];


int
main(void)
{
  std::cin.tie(0);
  std::ios::sync_with_stdio(false);

  REP (i, N) REP (j, i + 1) {
    std::cin >> array[i][j];
  }
  std::printf("answer = %u\n", search(0, 0));
  return EXIT_SUCCESS;
}


uint
search(uint depth, uint col)
{
  if (depth == LENGTH(array) - 1) {
    return array[LENGTH(array) - 1][col];
  } else {
    uint sum1 = search(depth + 1, col)     + array[depth][col];
    uint sum2 = search(depth + 1, col + 1) + array[depth][col];
    return std::max(sum1, sum2);
  }
}
